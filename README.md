# Webpack Local Dependency Resolution

Sometimes you wish to resolve a dependency locally without installing an npm package or creating symlinks.

This can be done using Webpack `alias` field.

This template demonstrates binding a dependency entry point to an alias of your choice.

## Set up

1. Clone repo.
2. Navigate to  `app`.
3. Run `npm i`, or its equivalent.
4. Run `npm start`, or its equivalent.
5. Consult console output for the port at which the page is served (8080 or close by).

## Repo Structure

There are two directories:
- **app**, which simulates an app that depends on...
- **dependency**, which we are going to alias locally in our app.

**If in a hurry, just consult `app/webpack.config.mjs`.**

## Caveat

Local dependency, when imported and bundled as a module of your app,
will import its **own** node modules by default. This will raise errors.
The solution is to provide aliases for the dependency's dependencies as well.
Clearly, this may become impractical if there are too many of them.

An alternative would be to publish a package. (Or perhaps to use `npm link`.)
For simple demos, however, I feel that the approach described here is the most practical.