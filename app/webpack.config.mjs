import { resolve, dirname } from "path";
import { fileURLToPath } from 'url';
const __dirname = dirname(fileURLToPath(import.meta.url));
export default (env, argv) => ({
  entry: {
    "main": "./src/main.ts",
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/,
        exclude: /node_modules/,
        use: ["babel-loader"],
      },
      {
        test: /^@/,
        exclude: /node_modules/,
        use: ["babel-loader"]
      }
    ],
  },
  resolve: {
    extensions: ["*", ".js", ".jsx", ".ts", ".tsx"],
    alias: argv.mode === "development"
    ?  {
      // resolve library locally
      "@dependency": resolve(__dirname, "../dependency/src/index.ts"),
      // rewire mutual dependencies of library and showcase
    }
    : {
      // in production
      // do nothing - library needs to be installed
    }
  },
  output: {
    path: resolve(__dirname, `./public`),
    filename: "[name].js",
  },
  devServer: {
    static: resolve(__dirname, `./public`),
  },
});